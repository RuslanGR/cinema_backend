# Cinema project (backend)

### Setup database
```
$ sudo su - postgres
$ psql
# CREATE USER cinema WITH SUPERUSER ENCRYPTED PASSWORD 'cinema';
# CREATE DATABASE cinema WITH OWNER 'cinema';
# \q
```

### Apply django migrations
```
python manage.py migrate
```