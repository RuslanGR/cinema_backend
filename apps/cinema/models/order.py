from django.db import models

from .visitor import Visitor
from .session import Session


class Order(models.Model):
    """Order model"""

    visitor = models.ForeignKey(Visitor, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)

    class Meta:
        """Meta"""

        verbose_name = 'Билет'
        verbose_name_plural = 'Билеты'

    def __str__(self):
        return f'Order {self.id}'
