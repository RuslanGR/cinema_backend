from django.db import models

from .film import Film


class Session(models.Model):
    """Session model"""

    film = models.ForeignKey(Film, on_delete=models.CASCADE)
    time = models.DateTimeField(verbose_name='Время сеанса')
    price = models.DecimalField(verbose_name='Цена билета', max_digits=5, decimal_places=2)

    class Meta:
        """Meta"""

        verbose_name = 'Сенас'
        verbose_name_plural = 'Сеансы'

    def __str__(self):
        return f'{self.film.name} {self.price}'
