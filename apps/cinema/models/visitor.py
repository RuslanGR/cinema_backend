from django.db import models


class Visitor(models.Model):
    """Visitor model"""

    email = models.EmailField(verbose_name='E-mail')
    phone = models.CharField(verbose_name='Номер телеона', max_length=10)
    name = models.CharField(verbose_name='Имя', max_length=30, null=True)

    class Meta:
        """Meta"""

        verbose_name = 'Постетитель'
        verbose_name_plural = 'Посетители'

    def __str__(self):
        return self.email
