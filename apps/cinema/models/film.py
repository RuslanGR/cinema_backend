from django.db import models


class Film(models.Model):
    """Film model"""

    name = models.CharField(verbose_name='Название', max_length=60)
    description = models.TextField(verbose_name='Описание')
    genre = models.CharField(verbose_name='Жанр', max_length=30, null=True)
    is_active = models.BooleanField(verbose_name='В прокате', default=False)

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'

    def __str__(self):
        return self.name
