import names
import loremipsum
import random
import string
from datetime import datetime

from django.http import HttpResponse

from apps.cinema.models import (
    Visitor,
    Film,
    Order,
    Session,
)


def email_gen():
    chars = string.ascii_letters
    return ''.join([random.choice(chars) for _ in range(10)]) + '@gmail.com'


def phone_gen():
    return '89' + ''.join([str(random.randint(0, 9)) for _ in range(8)])


def get_sentence(size=0):
    sentence = loremipsum.get_sentence(start_with_lorem=False).replace("b'", "").replace("'", "")
    return sentence[:size] if size else sentence


def index(request):
    from time import time
    start = time()

    all_visitors = Visitor.objects.all()
    all_session = Session.objects.all()
    all_films = Film.objects.all()

    # Films generations
    films = []
    for _ in range(100):  # 100
        film = Film(name=get_sentence(40),
                    description=''.join([get_sentence() for _ in range(2)]),
                    genre=get_sentence(20)
                    )
        films.append(film)
    Film.objects.bulk_create(films, batch_size=500)

    # Visitors generation
    visitors = []
    for _ in range(2000):  # 6000
        visitor = Visitor(email=email_gen(),
                          phone=phone_gen(),
                          name=names.get_first_name()
                          )
        visitors.append(visitor)
    Visitor.objects.bulk_create(visitors, batch_size=500)

    # Session creation
    sessions = []
    for _ in range(600):  # 2000
        session = Session(film=random.choice(all_films),
                          time=datetime(2018,  # Year
                                        random.randint(1, 12),  # Month
                                        random.randint(1, 28),  # Day
                                        random.randint(3, 22),  # Hour
                                        random.choice([10, 30, 50]),  # Minute
                                        ),
                          price=random.randint(120, 300)
                          )
        sessions.append(session)
    Session.objects.bulk_create(sessions, batch_size=500)

    # Order creation   :3  \o/
    orders = []
    for _ in range(600):  # 2000
        order = Order(
            visitor=random.choice(all_visitors),
            session=random.choice(all_session),
        )
        orders.append(order)
    Order.objects.bulk_create(orders, batch_size=500)

    final_time = abs(round(start-time(), 3))
    return HttpResponse(f'<p style="color: green;">OK, Takes {final_time} seconds</p>')
