from rest_framework import serializers

from .models import (
    Film,
    Order,
    Session,
    Visitor,
)


class VisitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitor
        fields = ('id', 'email', 'phone', 'name')


class FilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Film
        fields = ('id', 'name', 'description', 'genre')


class SessionSerializer(serializers.ModelSerializer):
    film = FilmSerializer()

    class Meta:
        model = Session
        fields = ('id', 'film', 'time', 'price')


class OrderSerializer(serializers.ModelSerializer):
    visitor = VisitorSerializer()
    session = SessionSerializer()

    class Meta:
        model = Order
        fields = ('id', 'visitor', 'session')
