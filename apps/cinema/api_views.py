import datetime
from decimal import Decimal

from rest_framework.permissions import IsAuthenticated
from rest_framework import (
    generics,
    permissions,
    response,
)

from .models import (
    Film,
    Order,
    Session,
    Visitor,
)
from .serializers import (
    FilmSerializer,
    OrderSerializer,
    SessionSerializer,
    VisitorSerializer,
)


class FilmList(generics.ListCreateAPIView):
    queryset = Film.objects.filter(is_active=True)
    permission_classes = (permissions.AllowAny, )
    serializer_class = FilmSerializer


class FilmDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Film.objects.filter(is_active=True)
    permission_classes = (permissions.AllowAny,)
    serializer_class = FilmSerializer


class OrderList(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    permission_classes = (permissions.AllowAny, )
    serializer_class = OrderSerializer


class OrderDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = OrderSerializer


class SessionList(generics.ListCreateAPIView):
    queryset = Session.objects.all()
    permission_classes = (permissions.AllowAny, )
    serializer_class = SessionSerializer

    # 2012-04-23T18:25:43.511 Json time format
    def create(self, request, *args, **kwargs):
        data = request.data
        film = Film.objects.create(name=data['film']['name'],
                                   description=data['film']['description'],
                                   genre=data['film']['genre'])
        Session.objects.create(
            price=Decimal(data.get('price')),
            film=film,
            time=datetime.datetime.now(),
        )
        return response.Response(data={200: 'OK'})


class SessionDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Session.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = SessionSerializer


class VisitorList(generics.ListCreateAPIView):
    queryset = Visitor.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = VisitorSerializer


class VisitorDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Visitor.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = VisitorSerializer
