from django.contrib import admin

from ..models import (
    Visitor,
    Session,
    Film,
    Order
)

admin.site.register(Visitor)
admin.site.register(Session)
admin.site.register(Film)
admin.site.register(Order)
