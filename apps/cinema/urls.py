from django.urls import re_path
from rest_framework.authtoken import views

from .api_views import *


urlpatterns = (
    re_path(r'auth/', views.obtain_auth_token),

    re_path(r'^films/$', FilmList.as_view()),
    re_path(r'^films/(?P<pk>\d+)/$', FilmDetails.as_view()),

    re_path(r'^orders/$', OrderList.as_view()),
    re_path(r'^orders/(?P<pk>\d+)/$', OrderDetails.as_view()),

    re_path(r'^sessions/$', SessionList.as_view()),
    re_path(r'^sessions/(?P<pk>\d+)/$', SessionDetails.as_view()),

    re_path(r'^visitors/$', VisitorList.as_view()),
    re_path(r'^visitors/(?P<pk>[0-9]+)/$', VisitorDetails.as_view()),
)
